from django.conf.urls import url
from series import views
from .views import FileView

urlpatterns = [
    url(r'^series/$', views.serie_list),
    url(r'^series/(?P<pk>[0-9]+)/$', views.serie_detail),
    url(r'^upload/$', FileView.as_view(), name='file-upload'),
]
